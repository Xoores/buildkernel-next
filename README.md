buildkernel-next is a tool that automagically compiles kernel with initramfs that enables full disk encryption using LUKS/LVM and creates necessary EFI boot file.


* [ABOUT](#about)
    * [My usecase](#my-usecase)
* [FEATURES](#features)
    * [Easy setup](#easy-setup)
    * [ENV aware](#env-aware)
    * [Smart EFI backup](#smart-efi-backup)
    * [Smart firmware](#smart-firmware)
    * [Smart caching](#smart-caching)
    * [External modules signing](#external-modules-signing)
    * [Kernel .config validation](#kernel-config-validation)
* [INSTALLATION](#installation)
    * [Dependencies](#dependencies)
* [USAGE AND OPTIONS](#usage-and-options)
    * [First time run](#first-time-run)
* [CONFIGURATION VARIABLES](#configuration-variables)
    * [BOOT_TARGET](#boot_target)
    * [KEY_UUID](#key_uuid)
    * [KEY_FILE](#key_file)
* [TODO](#todo)
* [FAQ](#faq)
    * [Why just not use Dracut?](#why-just-not-use-dracut)
    * [You could've forked buildkernel and maintain it instead](#you-couldve-forked-buildkernel-and-maintain-it-instead)
* [ERROR REPORTING](#error-reporting)

# ABOUT
This is complete rewrite of [Sakakis buildkernel](https://github.com/sakaki-/buildkernel/blob/master/buildkernel). Sakakis original script depends on some unmaintained/custom packages (namely [sys-kernel/genkernel-next](https://github.com/Sabayon/genkernel-next/) and her staticgpg package) and I wanted something that is possible to maintain long term. I opted for using [sys-kernel/dracut](https://dracut.wiki.kernel.org) and its scripts for LVM/LUKS.

Many thanks to Sakaki for doing this in the first place. Unfortunately Sakaki had to step down from maintaining these scripts due to some IRL events. Sakaki also wrote an awesome [Gentoo EFI installation guide](https://wiki.gentoo.org/wiki/User:Sakaki/Sakaki's_EFI_Install_Guide) which I might "clone" and keep updated in the future as well.

⚠️ **IMPORTANT: Please note that this is an early release** ⚠️  
This means, that configuration variables can be changed (names, defaults, etc) according to feedback. I will try to keep it as stable as possible, but please keep in mind that until this notice is not removed, things can change :-) I will of course try to keep track of everything in CHANGELOG.



### My usecase
The script is written in following usecase in mind:

1. **I want full disk encryption** - and I mean truly FDE where there is not a single unencrypted byte of my data (apart from LUKS headers & GPT of course)
1. **Linux only** - I don't use dualboot since for me it is a PITA and I can get away with KVM for my needs. Also I would need to keep Microsoft keys in the secureboot.
1. **Encrypted LUKS keys** - LUKS keys need to be encrypted so they are not just in plaintext on some flashdisk. I use GPG for that - and I also keep the key and kernel on something like [Kingston DT2000](https://www.kingston.com/en/usb-flash-drives/datatraveler-2000-encrypted-usb-flash-drive) so even the bootloader EFI file is not accessible without a PIN (paranoid much, I know).
1. **Easy creation of multiple boot disks** - I'm obivously not "brave" enough to have only one USB thumbdrive with the only encryption key in the world in my pocket every day. That's why I like to have more than one and to be able to easily keep all of them "updated" when upgrading kernel. I will eventually push the helper script for just that but I have to clean it up a little bit first.
1. **No suspend-to-disk** - I never use suspend to disk as it takes too long to sleep/wakeup. Also with FDE it is a royal PITA as you have to "freeze" your LUKS volume and somehow unlock it after wakeup. Few years ago I played with that a little bit but I quickly found out that I would need to patch kernel quite heavily and I was too lazy to do that. Situation may have changed since but I did not really wanted this so I never checked.
1. **KISS** - I don't want to use this script for kernel configuration nor do I want to try to create any kind of installation script. I just want this to generate bootable EFI file that gets put where I want and I want this boot file to be able to unlock my disk & boot into my system. Nothing too smart, nothing fancy. Keep it minimal.
1. **Smaller is better** - This usually is not a thing in life, but in initramfs and boot image size I believe that smaller is indeed better. I tend to keep critical modules for boot compiled in kernel (as I cannot really rmmod them anyway) and I keep all the fancy stuff like Bluetooth drivers for controlling wifi-enabled coffee maker in modules to be loaded later during boot of real system.

Please keep in mind that I wrote this for my use case, so I've implemented and tested just the things I tend to use. If you want to extend the functionality you can either fork this, create a PR or create a feature request issue but keep in mind that I work on this in my free time and might not be able to react in realtime to these.

My boot setup is: `[NVMe] -> [LUKS] -> [LVM] -> (partitions)`. I also use Gentoo so many of the assumptions and defaults are Gentoo-aware. This means that while with Gentoo you can probably run this script with just one or two parameters, on other distros it may not be the case. I can add some distro detection in the future if anyone wants, but for now this is "good enough".



# FEATURES
Mainly description of things that are different from Sakakis buildkernel.

### Easy setup
For most of the things you don't need any configuration file or any special configuration. If you are booted in your system, buildkernel-next will try to get necessary information from /proc/cmdline (LUKS keyfile and its partition UUID) and it also autodetects LUKS root (from mountpoint of /)

I opted for autodetection of as many things as possible since I always struggled to put correct UUIDs to the right places in buildkernel config etc. I know that this is mainly my problem but I tend to automate things is possible. It also skips autodetect of certain variable if you specify it as a parameter (see below).

### ENV aware
All of the important variables can be overriden trough ENV variables so you don't need config file. This makes it easy to have multiple USB boot devices with just a small wrapper script.

It is also possible to use `--legacy-config` which will try to extract necessary config values from old Sakakis config.

### Smart EFI backup
I quite liked that Sakakis script kept backup of previous kernel version so I did this here as well. I also added the "backup" kernel to EFI boot manager so it is now easier to "revert" back to the previous version.

However I changed the behavior in a way that if you are booted from "backup", it will keep backup intact when rebuilding kernel. This way you don't have to remember to backup your "backup" in order to have bootable OS.

### Smart firmware
Sakakais buildkernel tried to put everything in initramfs (namely all kernel modules & firmware) which for me resulted in 300M+ EFI file. Not only this takes 30m to compile on my HW but it also takes 30s to just load from USB. This made experimenting with kernel and its modules kinda painfull and time-consuming endeavor.

I know why she had it this way - it is the safest way to ensure that you can boot but the tradeoff is size and time.

I chose to include only firmware files that are necessary. I do that by searching dmesg for messages containing firmware loading messages. This is, of course, not foolproof; it breaks partially when booting for the first time (no firmware is available) or when your dmesg buffer rotates. I will add cache tied to kernel .config to solve the latter - until then the [SMART_FIRMWARE](#smart_firmware) is disabled by default.

### Smart caching
I included naive (but seems good for the use) caching of necessary files so I can avoid re-doing tasks when re-compiling EFI image because some secondary module changed (Virtualbox, nvidia etc).

After kernel compilation I just do a SHA sum of .config and save it. In subsequent runs, I just check and if the checksum is unchanged and if I still have necessary files, I don't rebuild. This also works for dracut image - if kernel did not change, dracut image does not need to be rebuilt. Saves some time, but honestly I just like the idea - I don't really think that I will save the planet by not compiling kernel & dracut :-)

This can, of course, be overriden by env variable or CLI parameter.

### External modules signing
I have some external modules and I use module signing. This means that unsigned modules are out of luck. This script will run through all the external modules and signs them so they can be loaded.

You can also run this script with `--sign-modules` option and it will just (re)sign external modules. Can be used in after-emerge hooks.

This feature will be automatically enabled when it detects CONFIG_MODULE_SIG=y in your .config.

### Kernel .config validation
Sakakis original script tried to ensure that all the necessary .config options are set, however she missed some - especially those necessary to boot LUKS/LVM as Crypto modules on Gentoo linux were not set by default. That frustrated me for a while so I took this oportunity and (hopefully) fixed it.

I also got bothered by tiny letters on my laptops 4k monitor during boot. I've found out that you need to enable bigger fonts in .config so I can share my joy by doing that for you as well (you can, of course, disable this with [HIDPI](#hidpi) variable)

# INSTALLATION
Installation is quite straightforward - this whole thing is written as a single file that can be placed anywhere you want.

### Gentoo
Recommended approach is to use `sys-kernel/buildkernel-next` ebuild available in [my overlay](https://gitlab.com/Xoores/gentoo-overlay/).

### Dependencies
buildkernel-next depends on following Gentoo packages:
* sys-kernel/dracut
* sys-boot/efibootmgr
* app-arch/cpio
* sys-apps/util-linux (lsblk, blkid)
* Some obvious ones like grep/sed/awk etc.

I use Gentoo names because these are what I'm familiar with, if you happen to know correct names for Debian/Fedora, please let me know and I will list them here as well.

This section is kinda work-in-progress, I may have forgotten something.


# USAGE AND OPTIONS
	Usage: buildkernel-next [-h] [-v] [-d] [options]
	 
	   Options
	     -h, --help			Show this help message and exit
	     -v, --version		Show version and exit
	     --show-config		Display effective configuration and exit
	     --diff-cmdline		Display diff of booted/.config cmdline and exit
	     --legacy-config		Parses /etc/buildkernel.conf and tries to use its values
	 
	   Logging options
	     -d, --debug		Enable debugging messages
	 
	   Workflow override options
	     --skip-kernel		Skip kernel & modules make
	     --skip-initramfs		Skip dracut & initramfs magic
	     --skip-install		Skip EFI file installation (it still will be created)
	 
	     --rebuild-kernel		Force kernel rebuild even if .config did not change
	     --rebuild-initramfs		Force initramfs rebuild
	 
	     --no-backup		Don't backup EFI file (bootx64.efi -> bootx64.efi.old)

### First time run
For the first time I would suggest to run `buildkernel-next --show-config` and check its output for obvious errors.

If you have used Sakakis buildkernel, you might also want to include `--legacy-config` option to parse /etc/buildkernel.conf

If you encounter a problem, try running with `--debug` option and see what's wrong. If you can't figure it out, you can submit an issue and if I can, I'll try to help (see [error reporting section](#error-reporting))


# CONFIGURATION VARIABLES
These are the variables you can use to tweak behaviour of buildkernel-next. All of these are passed through ENV so you can run something like `OPTION1="value1" buildkernel-next`. Mandatory/important variables are on top and in the ToC.


### BOOT_TARGET
*Accepts: Any unique identificator from blkid or fstab*  
*Mandatory: Yes if in chroot*  

Your boot disk. This is the disk that will have /EFI directory and your BIOS will try to boot from it.

### KEY_UUID
*Accepts: Any unique identificator from blkid*  
*Mandatory: No*  
*Default: ${BOOT_TARGET}*  

Disk where your LUKS keyfile sits. If not specified, it is assumed that your keyfile sits on the same partition as EFI file (BOOT_TARGET).

If ommited, LUKS passphrase will be required instead.

### KEY_FILE
*Mandatory: No*  
*Default: luks.gpg*  

Relative path of your LUKS keyfile on your disk identified by [KEY_UUID](#key_uuid).

If ommited, LUKS passphrase will be required instead.

### KERNEL_CMDLINE
*Mandatory: No*  
*Default: (empty)*  

Additional parameters for kernel cmdline. I like to put `net.ifnames=0` there, for example.

### DRACUT_PARAMS
*Mandatory: No*  
*Default: (empty)*  

Additional parameters for Dracut - for example if you want Plymouth, you can add it with this.


### MODULES_SIGN_KEY
*Accepts: Existing file with x509 private key*  
*Mandatory: No*  
*Default: /usr/src/linux/certs/signing_key.x509*  

Key for module signing.

### MODULES_SIGN_CERT
*Accepts: Existing file with pem cert*  
*Mandatory: No*  
*Default: /usr/src/linux/certs/signing_key.pem*  

Certificate for module signing.

### SECUREBOOT_CERT
*Accepts: Existing file with pem cert*  
*Mandatory: No*  
*Default: /etc/efikeys/db.crt*  

Certificate for Secureboot.

### SECUREBOOT_KEY
*Accepts: Existing file with pem key*  
*Mandatory: No*  
*Default: /etc/efikeys/db.key*  

Private key for Secureboot.


### REAL_ROOT
*Accepts: /dev/ path to your rootfs (/)*  
*Mandatory: No*  

Path to device you mount to /. Don't really need to worry about this as it works even from chroot - it is detected from mount(8).

### REALROOT_FSTYPE
*Accepts: fstype accepted by mount(8)*  
*Mandatory: No*  
*Default: ext4*  

Filesystem of your /. It is autodetected, so no need to worry.

### MAKEOPTS
*Mandatory: No*  
*Default: -j${NUMCPUSPP} -l${NUMCPUS}*  

Options passed to make(1). Default is usual Gentoo "number of CPU cores" and "CPU cores + 1"

### KERNELDIR
*Accepts: existing directory*  
*Mandatory: No*  
*Default: /usr/src/linux*  

Directory that contains kernel sources. On gentoo with symlinks it is /usr/src/linux for selected kernel.

### TEMP_CPIODIR
*Accepts: existing directory*  
*Mandatory: No*  
*Default: /tmp/bootwrapper*  

Directory that buildkernel-next uses for updating initramfs generated by Dracut. The best strategy is tmpfs, but it really does not matter that much.

### DISABLE_LVM
*Accepts: 0|1*  
*Mandatory: No*  
*Default: 0*  

Disables LVM

### HIDPI
*Accepts: 0|1*  
*Mandatory: No*  
*Default: 1*  

Enables HiDPI configuration options (bigger font for initial framebuffer)

### REAL_INIT
*Accepts: existing file*  
*Mandatory: No*  
*Default: /sbin/init*  

Your initsystem. If you have SystemD, you will have to set this.


### SMART_FIRMWARE
*Accepts: 0|1*  
*Mandatory: No*  
*Default: 0*  

Enables or disables [Smart firmware](#smart-firmware) functionality. By default it is disabled because there is a known limitation for now that I need to address (see [TODO](#cache-for-firmware-files))

### DISABLE_EFI_BOOTMGR
*Accepts: 0|1*  
*Mandatory: No*  
*Default: 0*  

Enables or disables EFI bootloader configuration using efibootmgr. If enabled, buildkernel-next will ensure that primary and backup kernel are added as boot options while selecting primary kernel as primary boot target.

### DISABLE_EFI_BACKUP
*Accepts: 0|1*  
*Mandatory: No*  
*Default: 0*  

Enables or disables backups of EFI files.



# TODO
Features that are on my TODO list. I don't think that these are critical, you can easily use this as it is now but there is always something that can be improved. Most important are on top.

### Cache for firmware files
As explained above, Smart firmware is smart until your dmesg buffer rolls over. After that you are SoL since it cannot find any firmware anymore. This was not an issue during development of this since I rebooted quite a lot but now...

I will add some list of necessary firmware files tied to kernel .config and I will probably just assume that if config stays the same, the HW will stay the same. This will work for people like me that compile only necessary modules but the "build'em all" folks will probably need to disable this by setting SMART_FIRMWARE to 0.

### Log to file
It would be nice to create full log of last run somewhere so it can be studied later. Bonus points for "anonymized" logs that hide some potentionally sensitive information.

### Detection of missing packages/commands
Self-explainatory. It would be nice to check that we have all packages installed and error-out if not.

### Add systemd config option
Right now this script is used with OpenRC and if you want SystemD support, you have to 1) set appropriate .config options and 2) change [REAL_INIT](#real_init) variable.

It would be nice to have one option that would enable to choose from SystemD/OpenRC.

# FAQ
Nobody actually asked any of these so let me do that for you now so I can preemptively provide some unsolicited answers :-)

### Why just not use Dracut?
Dracut is the "heavy lifter" in this script, however it needs non-trivial amount of configuration and I also wanted some easy customization of created initramfs. This script will try to guess almost all the necessary variables/settings and run with it.

### You could've forked buildkernel and maintain it instead
Yeah, maybe. I thought about this for a while but when I went through Sakakis original script, I quickly realized that it has many features I don't care about (for example "interactive mode" etc) and that it would take quite a lot of work to comb through all of that. And it would be quite hard for me not to break anything. While I was at it I also went "full BASH" and used some functions that are available in the newer BASH versions (4.0+) since the primary system - atleast for me - would be Gentoo.

TL;DR: It was easier for me to rewrite the whole thing instead of trying to edit 2300+ lines of script that has a lot of legacy features that I don't care for.


# ERROR REPORTING
Please keep in mind that even though I try to test this app, I have only limited time to do so and I only test on my setup. That means that I never have a chance to test e.g. mdraid or pymouth or any of the thousands possible modules.

If you want some advanced feature that I don't have access to/use, or you encounter an error, you can:
1. Create an issue here in hopes that I get around and implement said feature
1. Try to implement it yourself (and create merge request if possible so others can benefit from your suffering as well)
1. Use [Sakakis buildkernel](https://github.com/sakaki-/buildkernel/blob/master/buildkernel)

I will try to keep this thing updated and working as there are few people in my circles that use this as well, but I cannot guarantee anything. If you really want to help me narrow down an issue, please try to include as much information as you can.

That means it would be nice if you could submit full log witn `--debug` option enabled.

When buildkernel-next ecnounters an error or bug, it will complain and it will also dump all important variables. Bug encounter will be accopmanied with more detailed info such as stacktrace and some more variables being dumped. Please include all this information if possible.