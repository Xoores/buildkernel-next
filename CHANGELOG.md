# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [0.1.1] - 2022-02-15
### Added
- Changelog :-)
- Option to parse legacy config file `--legacy-config`

### Changed
- `SMART_FIRMWARE` is disabled by default
- Changed option `NO_EFI_BOOTMGR` to `DISABLE_EFI_BOOTMGR`
- Changed option `NO_EFI_BACKUP` to `DISABLE_EFI_BACKUP`
- Changed option `LVM` to `DISABLE_LVM`


## [0.1.0] - 2022-02-14
### Added
- First release and upload to git